﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ExcelTransferData.Classes
{

    /// <summary>
    /// This class works along with MainWindow.xaml.cs
    /// ---------------------------------------------------
    /// REMINDER :
    /// 1) It interacts with System.Data.DataTable
    /// 2) The project must add reference to Microsoft.Office.Interop.Excel
    /// 3) The accessing folder must be able to access all user
    /// 4) All Excel files must be closed and processes ended
    /// ---------------------------------------------------
    /// </summary>
    class AccessExcel
    {
        private Range xlRange;
        private Sheets sheets;
        private Worksheet ws;
        private Workbook wb;
        private Workbooks wbs;
        private Application xlApp;

        private int count = 0;

        public AccessExcel()
        {
            xlApp = new Application();
            xlApp.Visible = false;
            xlApp.DisplayAlerts = false;
            wbs = xlApp.Workbooks;
        }

        public void CreateNewFile(string Newfilepath, string NewSheetName = "Sheet1")
        {
            try
            {
                wb = xlApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
                ws = wb.Worksheets[1];
                ws.Name = NewSheetName;
                wb.SaveAs(Newfilepath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                     false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is not opened."));
            }
            catch
            {
                throw new Exception(string.Format("Error occurred.\nPossible reasons :\n- " +
                    "Sheet index is not valid\n- Etc."));
            }
        }

        public void OpenWorkBook(string filepath)
        {
            try
            {
                wb = wbs.Open(filepath);
            }

            /*catch (System.Runtime.InteropServices.COMException ce)
            {
                //0x800AC472
                if ((uint)ce.ErrorCode == 0x800AC472&&count<=3)
                {
                    System.Windows.MessageBox.Show("Fk excel");
                    xlApp = new Application();
                    count++;
                    System.Threading.Thread.Sleep(200);
                    OpenWorkBook(filepath);
                }
                else
                {
                    throw new Exception(string.Format("Number of tries to open Excel Application : {0}", count));
                }

            }*/
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error message :\n{0}\nYou may try to restart the application or wait for awhile.", ex.Message));
            }
        }

        //Open sheet by sheet index
        public void OpenSheet(int SheetIndex)
        {
            try
            {
                ws = wb.Sheets[SheetIndex];
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is not opened."));
            }
            catch
            {
                throw new Exception(string.Format("Error occurred.\nPossible reasons :\n- " +
                    "Sheet index is not valid\n- Etc."));
            }
        }
        //Open sheet by sheet name
        public void OpenSheet(string SheetName)
        {
            try
            {
                ws = wb.Sheets[SheetName];
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is not opened."));
            }
            catch
            {
                throw new Exception(string.Format("Error occurred.\nPossible reasons :\n- " +
                    "Sheet name is not valid\n- Sheet name is not found\n- Etc."));
            }
        }

        public System.Data.DataTable TableData
        {
            get
            {
                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    xlRange = ws.UsedRange;

                    for (int col = 1; col <= xlRange.Columns.Count; col++)
                    {
                        string strData = "";
                        try
                        {
                            strData += (string)(xlRange.Cells[1, col] as Microsoft.Office.Interop.Excel.Range).Value2;
                        }
                        catch
                        {
                            strData += (xlRange.Cells[1, col] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
                        }
                        strData = strData.Replace(")", "");
                        dt.Columns.Add(strData, typeof(string));

                    }
                    string strCellData = "";
                    double douCellData;
                    for (int rowCnt = 2; rowCnt <= xlRange.Rows.Count; rowCnt++)
                    {
                        string strData = "";
                        for (int colCnt = 1; colCnt <= xlRange.Columns.Count; colCnt++)
                        {
                            Boolean can = false;
                            try
                            {
                                strCellData = (string)(xlRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Formula;
                                strData += strCellData + "|";
                                can = false;
                            }
                            catch (Exception) { can = true; }

                            if (can)
                                try
                                {
                                    strCellData = (string)(xlRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                                    strData += strCellData + "|";
                                }
                                catch
                                {
                                    douCellData = (xlRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                                    strData += douCellData.ToString() + "|";
                                }

                        }
                        strData = strData.Remove(strData.Length - 1, 1);
                        strData = strData.Replace("\\", "\\\\");
                        dt.Rows.Add(strData.Split('|'));
                    }
                    return dt;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
                }
            }
        }

        public System.Data.DataTable SheetList
        {
            get
            {
                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("SheetName");
                    foreach (Worksheet tempws in wb.Worksheets)
                    {
                        dt.Rows.Add(tempws.Name);
                    }
                    return dt;
                }
                catch (NullReferenceException)
                {
                    throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
                }
            }
        }

        public void SaveFile()
        {
            try
            {
                wb.Save();
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void SaveAsFile(string filepath)
        {
            try
            {
                wb.SaveAs(filepath);
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void AddNewSheet(string SheetName)
        {
            try
            {
                sheets = wb.Worksheets;
                ws = (Worksheet)sheets.Add(After: wb.Sheets[wb.Sheets.Count]);
                ws.Name = SheetName;
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void RenameSheet(string oldName, string newName)
        {
            try
            {
                ws = (Worksheet)wb.Worksheets[oldName];
                ws.Name = newName;
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void UpdateWorkSheet(System.Data.DataTable datatable)
        {
            int row = 1, col = 1;
            try
            {
                foreach (DataColumn dc in datatable.Columns)
                {
                    ws.Cells[row, col] = dc.ColumnName;
                    col++;
                }

                //get rows
                for (row = 2; row < datatable.Rows.Count; row++)
                    for (col = 1; col < datatable.Columns.Count; col++)
                        ws.Cells[row, col] = datatable.Rows[row].ItemArray.GetValue(col);
                ws.Columns.AutoFit();
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void CloseBook()
        {
            try
            {
                wb.Close();
            }
            catch (NullReferenceException)
            {
                throw new Exception(string.Format("Error occurred.\nWorkbook is empty."));
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error occurred.\nError message :\n{0}", ex.Message));
            }
        }

        public void Release_Resources()
        {

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            if (xlRange != null) { Marshal.ReleaseComObject(xlRange); }
            if (ws != null) { Marshal.ReleaseComObject(ws); }
            if (sheets != null) { Marshal.ReleaseComObject(sheets); }
            if (wb != null) { try { wb.Close(); } catch { } finally { Marshal.ReleaseComObject(wb); } }
            if (wbs != null) { try { wbs.Close(); } catch { } finally { Marshal.ReleaseComObject(wbs); } }
            if (xlApp != null) { try { xlApp.Quit(); } catch { } finally { Marshal.ReleaseComObject(xlApp); } }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            kill_process("Excel");
        }

        private void kill_process(string processName)
        {
            Process[] process = Process.GetProcessesByName(processName);
            foreach (Process prc in process)
                prc.Kill();
        }
    }
}
