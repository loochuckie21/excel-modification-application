﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ExcelTransferData
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MessageBox.Show("Please ensure that all excel files are saved and closed before you continue to use this application",
                "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            Thread.Sleep(10);
        }
    }
}
