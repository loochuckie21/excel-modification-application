﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFLearn.CustomizedControl
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog()
        {
            InitializeComponent();
            txtHeader.Focus();
            txtHeader.SelectAll();
        }

        public InputDialog(string command)
        {
            InitializeComponent();
            lblcommand.Text = command;
            txtHeader.Focus();
            txtHeader.SelectAll();
        }
        public InputDialog(string command,string title)
        {
            this.Title = title;
            InitializeComponent();
            lblcommand.Text = command;
            txtHeader.Focus();
            txtHeader.SelectAll();
        }
        public string inputText
        {
            get{ return txtHeader.Text; }
            set{ txtHeader.Text = value; }
        }

        private void Btn_OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            e.Handled = true;
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult= false;
            e.Handled = true;
        }

        private void TxtHeader_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                Btn_OK_Click(sender,e);
            if (e.Key == Key.Escape)
                Btn_Cancel_Click(sender, e);
        }
    }
}
