﻿
using System.Windows;

namespace ExcelTransferData.CustomControl
{
    /// <summary>
    /// Interaction logic for Operation.xaml
    /// </summary>
    public enum Operators
    {
        Add, Minus, Divide, Multiply
    }
    public partial class OperationDialog : Window
    {
        public Operators Operations
        {
            get { return Op; }
        }

        private Operators Op { get; set; }

        public OperationDialog()
        {
            InitializeComponent();
        }

        public OperationDialog(string command)
        {
            InitializeComponent();
            lblCommand.Text = command;
        }

        public OperationDialog(string command, string title)
        {
            InitializeComponent();
            lblCommand.Text = command;
            this.Title = title;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Op = Operators.Add;
            e.Handled = true;
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Op = Operators.Minus;
            e.Handled = true;
        }

        private void Multiply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Op = Operators.Multiply;
            e.Handled = true;
        }

        private void Divide_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Op = Operators.Divide;
            e.Handled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            e.Handled = true;
        }
    }
}
