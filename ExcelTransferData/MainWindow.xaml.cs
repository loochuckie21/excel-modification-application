﻿using ExcelTransferData.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WPFLearn.CustomizedControl;

namespace ExcelTransferData
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private AccessExcel exc = new AccessExcel();
        private string filename;
        List<StoreData> SelectedData = new List<StoreData>();
        Boolean CopyToggle = false;
        Boolean AppendToggle = false;
        Boolean OperateToggle = false;


        private DataTable leftDT, rightDT;


        public MainWindow()
        {
            InitializeComponent();
            #region Generate empty column
            //DataTable leftDT = new DataTable();

            //leftDT.Columns.Add();
            //for (int colCnt = 1; colCnt <= 20; colCnt++)
            //{
            //    string strColumn = "";
            //    int ASCII = 65;
            //    int remainder = colCnt - 1;
            //    do
            //    {
            //        if (remainder / 26 > 0)
            //        {
            //            ASCII += (remainder / 26) - 1;
            //            strColumn += (char)ASCII;
            //        }
            //        ASCII = 65;
            //        ASCII += (remainder % 26);
            //        strColumn += (char)ASCII;
            //        remainder -= 26;
            //    } while (remainder - 26 > 0);

            //    leftDT.Columns.Add(strColumn, typeof(string));
            //}
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //leftDT.Rows.Add();
            //LExcelView.ItemsSource = leftDT.DefaultView;
            #endregion
        }

        #region left wing

        private void LExcelView_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void ExcelView_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            var edg = sender as DataGrid;
            DataTable dt = ((DataView)edg.ItemsSource).ToTable();

            if (!CopyToggle && !OperateToggle && !AppendToggle)
            {
                IList<DataGridCellInfo> items = edg.SelectedCells;
                SelectedData.Clear();
                try
                {
                    foreach (DataGridCellInfo a in items)
                    {
                        int row = edg.Items.IndexOf(a.Item);
                        int col = a.Column.DisplayIndex;
                        string val = dt.Rows[row].ItemArray.GetValue(col).ToString();
                        SelectedData.Add(new StoreData(row, col, val));
                    }
                }
                catch { }
            }
            //MessageBox.Show(String.Format("row : {0} , column : {1}", edg.Items.IndexOf(edg.CurrentCell.Item).ToString(), edg.CurrentColumn.DisplayIndex));
            if (CopyToggle)
            {
                StoreData copyDesti = new StoreData(edg.Items.IndexOf(edg.CurrentCell.Item), edg.CurrentColumn.DisplayIndex);
                int diffRow = SelectedData.ElementAt(0).RowIndex - copyDesti.RowIndex;
                int diffCol = SelectedData.ElementAt(0).ColumnIndex - copyDesti.ColumnIndex;
                foreach (StoreData tempData in SelectedData)
                {
                    if (dt.Rows.Count <= tempData.RowIndex - diffRow)
                        for (int i = dt.Rows.Count; i <= tempData.RowIndex - diffRow; i++)
                            dt.Rows.Add();
                    if (dt.Columns.Count <= tempData.ColumnIndex - diffCol)
                        for (int i = dt.Columns.Count; i <= tempData.ColumnIndex - diffCol; i++)
                            dt.Columns.Add();
                    dt.Rows[tempData.RowIndex - diffRow][tempData.ColumnIndex - diffCol] = tempData.Value;
                }
                CopyToggle = false;
                TBcopy.Content = "Copy";

                TBAppend.IsEnabled = TBoppr.IsEnabled = true;
                SelectedData.Clear();
                LExcelView.UnselectAll();
                RExcelView.UnselectAll();
                LExcelView.UnselectAllCells();
                RExcelView.UnselectAllCells();
                edg.ItemsSource = dt.DefaultView;
                txtHint.Text = "Copied";
            }
            if (AppendToggle)
            {
                StoreData copyDesti = new StoreData(edg.Items.IndexOf(edg.CurrentCell.Item), edg.CurrentColumn.DisplayIndex);
                string tempResult = "";
                foreach (StoreData tempData in SelectedData)
                {
                    tempResult += tempData.Value;
                }
                dt.Rows[copyDesti.RowIndex][copyDesti.ColumnIndex] = tempResult;
                AppendToggle = false;
                TBAppend.Content = "Append";
                TBcopy.IsEnabled = TBoppr.IsEnabled = true;
                SelectedData.Clear();
                LExcelView.UnselectAll();
                RExcelView.UnselectAll();
                LExcelView.UnselectAllCells();
                RExcelView.UnselectAllCells();
                edg.ItemsSource = dt.DefaultView;
                txtHint.Text = "Append completed.";
            }
            if (OperateToggle)
            {
                StoreData copyDesti = new StoreData(edg.Items.IndexOf(edg.CurrentCell.Item), edg.CurrentColumn.DisplayIndex);
                CustomControl.OperationDialog od = new CustomControl.OperationDialog();
                bool? yn = od.ShowDialog();
                double result = 0;
                try
                {
                    if (yn == true)
                    {
                        switch (od.Operations)
                        {
                            case CustomControl.Operators.Add:
                                {
                                    foreach (StoreData temp in SelectedData)
                                    {
                                        result += double.Parse(temp.Value);
                                    }
                                    break;
                                }
                            case CustomControl.Operators.Minus:
                                {
                                    result = double.Parse(SelectedData.ElementAt(0).Value);
                                    for (int i = 1; i < SelectedData.Count; i++)
                                    {
                                        result -= double.Parse(SelectedData.ElementAt(i).Value);
                                    }
                                    break;
                                }
                            case CustomControl.Operators.Multiply:
                                {
                                    result = double.Parse(SelectedData.ElementAt(0).Value);
                                    for (int i = 1; i < SelectedData.Count; i++)
                                    {
                                        result *= double.Parse(SelectedData.ElementAt(i).Value);
                                    }
                                    break;
                                }
                            case CustomControl.Operators.Divide:
                                {
                                    result = double.Parse(SelectedData.ElementAt(0).Value);
                                    for (int i = 1; i < SelectedData.Count; i++)
                                    {
                                        result /= double.Parse(SelectedData.ElementAt(i).Value);
                                    }
                                    break;
                                }
                        }
                        dt.Rows[copyDesti.RowIndex][copyDesti.ColumnIndex] = result;
                        OperateToggle = false;
                        TBoppr.Content = "Operation";
                        TBcopy.IsEnabled = TBAppend.IsEnabled = true;
                        SelectedData.Clear();
                        LExcelView.UnselectAll();
                        RExcelView.UnselectAll();
                        LExcelView.UnselectAllCells();
                        RExcelView.UnselectAllCells();
                        edg.ItemsSource = dt.DefaultView;
                        txtHint.Text = "Operation completed.";
                    }
                    else
                        return;
                }
                catch (FormatException)
                {
                    MessageBox.Show(string.Format("Error occured.\nThe selected cell(s) might contain alphabetic characters."));
                }
            }

        }

        private void LBtnNewRow_Click(object sender, RoutedEventArgs e)
        {
            var edg = LExcelView as DataGrid;
            DataTable leftDT = ((DataView)LExcelView.ItemsSource).ToTable();
            try
            {
                edg.IsEnabled = false;
                leftDT.Rows.Add();
                edg.ItemsSource = leftDT.DefaultView;
                edg.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LBtnNewHeader_Click(object sender, RoutedEventArgs e)
        {
            DataGrid edg = LExcelView as DataGrid;
            DataTable dt = ((DataView)edg.ItemsSource).ToTable();
            try
            {
                edg.IsEnabled = false;
                dt.Columns.Add();
                edg.ItemsSource = dt.DefaultView;
                edg.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LBtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGrid ledg = LExcelView as DataGrid;
                Microsoft.Win32.FileDialog fdExcel = new Microsoft.Win32.OpenFileDialog();
                fdExcel.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                fdExcel.Filter = "Excel File (*.xls;*.xlsx;)|*.xls;*.xlsx;";
                bool? yn = fdExcel.ShowDialog();
                if (yn == true)
                    filename = fdExcel.FileName;
                else
                    return;

                exc.OpenWorkBook(filename);
                exc.OpenSheet(1);
                LcmbSheets.ItemsSource = exc.SheetList.DefaultView;
                LcmbSheets.DisplayMemberPath = "SheetName";
                LcmbSheets.SelectedIndex = 0;
                leftDT = exc.TableData;
                ledg.ItemsSource = leftDT.DefaultView;
                string DisFolder = filename.Remove(filename.LastIndexOf("\\"));
                string[] tempfile = filename.Split('\\');
                string DisFile = tempfile[tempfile.Length - 1];
                LFolderName.Text = DisFolder;
                LFileName.Text = DisFile;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LFolderName.Text = "";
                LFileName.Text = "";
            }
        }

        private void LBtnEditHeader_Click(object sender, RoutedEventArgs e)
        {
            DataGrid edg = LExcelView as DataGrid;
            try
            {
                int col = edg.SelectedCells[0].Column.DisplayIndex;
                DataTable dt = ((DataView)edg.ItemsSource).ToTable();
                InputDialog input = new InputDialog("Enter new header name");
                string newHeader;
                bool? yn = input.ShowDialog();
                if (yn == true)
                    newHeader = input.inputText;
                else
                    return;
                dt.Columns[col].ColumnName = newHeader;
                edg.ItemsSource = dt.DefaultView;
                edg.UnselectAll();
                edg.UnselectAllCells();
            }
            catch
            {
                MessageBox.Show(String.Format("Please select the any of the column that you wish to edit the header."));
                return;
            }
        }

        #endregion
        //end left wing


        #region Center
        private void TBcopy_Click(object sender, RoutedEventArgs e)
        {
            if (TBcopy.Content.Equals("Copy"))
            {
                if (SelectedData.Count == 0)
                {
                    MessageBox.Show("Please select any cells first.");
                }
                else
                {
                    TBcopy.Content = "Cancel Copy";
                    CopyToggle = true;
                    AppendToggle = OperateToggle = false;

                    TBAppend.IsEnabled = TBoppr.IsEnabled = false;
                    txtHint.Text = "Click on the cell that you want to paste.";
                }
            }
            else
            {
                TBcopy.Content = "Copy";
                TBoppr.IsEnabled = TBAppend.IsEnabled = true;
            }
        }

        private void TBAppend_Click(object sender, RoutedEventArgs e)
        {
            if (TBAppend.Content.Equals("Append"))
            {
                if (SelectedData.Count == 0)
                {
                    MessageBox.Show("Please select any cells first.");
                }
                else if (SelectedData.Count < 2)
                {
                    MessageBox.Show("Please select atleast 2(TWO) cells to append.");
                }
                else
                {
                    TBAppend.Content = "Cancel Append";
                    AppendToggle = true;
                    CopyToggle = OperateToggle = false;

                    TBcopy.IsEnabled = TBoppr.IsEnabled = false;
                    txtHint.Text = "Click on the cell that you want to show the result.";
                }
            }
            else
            {
                TBAppend.Content = "Append";
                TBoppr.IsEnabled = TBcopy.IsEnabled = true;
            }
        }

        private void TBoppr_Click(object sender, RoutedEventArgs e)
        {
            if (TBoppr.Content.Equals("Operation"))
            {
                if (SelectedData.Count == 0)
                {
                    MessageBox.Show("Please select any cells first.");
                }
                else if (SelectedData.Count < 2)
                {
                    MessageBox.Show("Please select atleast 2(TWO) cells to operate.");
                }
                else
                {
                    TBoppr.Content = "Cancel Operate";
                    OperateToggle = true;
                    CopyToggle = AppendToggle = false;

                    TBcopy.IsEnabled = TBAppend.IsEnabled = false;
                    txtHint.Text = "Click on the cell that you want to show the result.";
                }
            }
            else
            {
                TBoppr.Content = "Operation";
                TBcopy.IsEnabled = TBAppend.IsEnabled = true;
            }
        }
        #endregion
        //end center


        #region right wing
        private void RBtnNewRow_Click(object sender, RoutedEventArgs e)
        {
            var edg = RExcelView as DataGrid;
            rightDT = ((DataView)RExcelView.ItemsSource).ToTable();
            edg.IsEnabled = false;
            rightDT.Rows.Add();
            edg.ItemsSource = rightDT.DefaultView;
            edg.IsEnabled = true;

        }

        private void RExcelView_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void RBtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (exc == null)
                    exc = new AccessExcel();

                DataGrid redg = RExcelView as DataGrid;
                Microsoft.Win32.FileDialog fdExcel = new Microsoft.Win32.OpenFileDialog();
                fdExcel.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
                fdExcel.Filter = "Excel File (*.xls;*.xlsx;)|*.xls;*.xlsx;";
                bool? yn = fdExcel.ShowDialog();
                if (yn == true)
                    filename = fdExcel.FileName;
                else
                    return;

                exc.OpenWorkBook(filename);
                exc.OpenSheet(1);
                RcmbSheets.ItemsSource = exc.SheetList.DefaultView;
                RcmbSheets.DisplayMemberPath = "SheetName";
                RcmbSheets.SelectedIndex = 0;
                rightDT = exc.TableData;
                redg.ItemsSource = rightDT.DefaultView;

                string DisFolder = filename.Remove(filename.LastIndexOf("\\"));
                string[] tempfile = filename.Split('\\');
                string DisFile = tempfile[tempfile.Length - 1];
                RFolderName.Text = DisFolder;
                RFileName.Text = DisFile;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                RFolderName.Text = "";
                RFileName.Text = "";
            }
        }

        private void RBtnNewHeader_Click(object sender, RoutedEventArgs e)
        {
            DataGrid edg = RExcelView as DataGrid;
            DataTable dt = ((DataView)edg.ItemsSource).ToTable();
            try
            {
                edg.IsEnabled = false;
                dt.Columns.Add();
                edg.ItemsSource = dt.DefaultView;
                edg.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RBtnEditHeader_Click(object sender, RoutedEventArgs e)
        {
            DataGrid edg = RExcelView as DataGrid;
            try
            {
                int col = edg.SelectedCells[0].Column.DisplayIndex;
                DataTable dt = ((DataView)edg.ItemsSource).ToTable();
                InputDialog input = new InputDialog("Enter new header name");
                string newHeader;
                bool? yn = input.ShowDialog();
                if (yn == true)
                    newHeader = input.inputText;
                else
                    return;
                dt.Columns[col].ColumnName = newHeader;
                edg.ItemsSource = dt.DefaultView;
                edg.UnselectAll();
                edg.UnselectAllCells();
            }
            catch
            {
                MessageBox.Show(String.Format("Please select the any of the column that you wish to edit the header."));
                return;
            }

        }
        #endregion
        //end right wing       

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            exc.Release_Resources();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            exc.Release_Resources();
        }














        // a class for storing row index, col index and value
        private class StoreData
        {
            private int row, col;
            private string val;

            public StoreData() { }
            public StoreData(int row, int col, string val)
            {
                this.RowIndex = row;
                this.ColumnIndex = col;
                this.Value = val;
            }
            public StoreData(int row, int col)
            {
                this.RowIndex = row;
                this.ColumnIndex = col;
            }

            public int RowIndex
            {
                get { return row; }
                set { row = value; }
            }
            public int ColumnIndex
            {
                get { return col; }
                set { col = value; }
            }
            public String Value
            {
                get { return val; }
                set { val = value; }
            }

        }
    }
}

